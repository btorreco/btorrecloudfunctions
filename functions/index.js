'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
const express = require('express');
const cors = require('cors')({ origin: true });

const app = express();

admin.initializeApp({
    /**
     * For production
    */
    credential: admin.credential.cert({
        "type": "service_account",
        "project_id": "torre-1df46",
        "private_key_id": "87d1ed2ca6f91c7661cd04a2f3e76ddccd76b47d",
        "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCpLVZiLtKvw7n0\nPn3tWdRLWTZHsysY9sZHzhUn6zu1NQVqpR1LaEbEGrD4jHrCjjnfvOK7YHFE88d1\n/r3Ymurhb99Nlc2mtrE4R2ts9p2ADz0wMremMjHMyLrxQIvdIxR0JMuaCtmXLSEA\n6DMPEH1ZjkXvYVXWsYVEHAyzKarSlIdJE/rRq67h+sbboX1X9D21FaqUTSbgqWIT\nEsbgxZ05hSD6kE/us7InPB35uHqptIuK9mUwCu5POE1Ed8ILOxkTR43BC2zpoAmk\nPhM8Om3fmVChdlKouP5EsXIXZ0G4w3myR1rGAQZI/Hfq1zIiUbCPNez69jMZsITG\ne1qn8SG9AgMBAAECggEAIj5YkWgb4lLyZWNu1ufLZnMI3yt/ij6NNQScWKevPscc\nMRF55AUX6eLjNnQs9wuV4FlZ5YaodsYHdjVZlSoKaBZec1Riu5+YMKWYM4aZq82p\n24MwlDXtyZ9J48JmbR0IK1g1VBllIMoUN5xIlYCHduSq+EDSALKPIJGy6mMBPj1Z\nAyozayZdgNP18sUJ43A3/mUkEAAPskshKuTpzFKsKKR8Ax4Agie5m9dc1TeTpRSs\n9wWLBbrwaldmA9xGthzedWmIDVqii/R4/7FaeqZrEPk5d7JHqWTKxd1j5kPJ4oBD\nagXpN+sB88GbMDBe9MtGEntJs9n1cwJ3EGxfAeSwOwKBgQDQcv/nWsa+5RB8tQnI\n1wv1LY3xYcwLs7zX8oI7zlUJArcmFPjlKV/tj79jRW9B2NfrS2wzv2YkvrjKpyTB\nt/RatuQtugwqT5HKIPR4NSQ4Y2Q2PUmG0iC5bRelXo/oM6H55QO4LJQ1L6grnIc4\nSM8XJ7Sjm4gc+W+q8v+l1Q1+IwKBgQDPxO2JNJv1W9f4aKt5GDP42syZdW2SlfUq\npiUisXoEILcPfGz/S9S+AoF+Gz5ZnqbDjOfz1b9jGGi7QmUtw9dQhRaWnUwgcThn\n5pbS7L8pCjoN4MJemPoCz3kc9fveXPV2YPdYRBp7zjg63UM1Gf2W0D0rje0GKEhz\n9AL1fxyunwKBgQChcU/tmYrwcmzwiS6I1LU4MQ4Mc5gbQBv0jKh9NXL6xZWf8wJK\nSl83mOu8MRzYPOAOJp4o/gZYUzbADfAT8StZzuiu2bduVa4uhJOTPmVNHv2QXKtD\nth8OPgXgWHrQdpzu9IaK8EvhXYW8MUExQILUBjaLuHwVJMNt/UIu5/HJTQKBgFii\nVAsyTE4RjkLEtFIGhzVkceANfl8IZfx5wGHagIj+Q9nxZLZMbUdZ3lAj4ydsTQwj\nMliNGQZ3RUaNfohVgacx/37lP7KfWkbI0hLNOIeTe0ysU37zl8RXKf222mn0Eisc\nFqiq9PlX8hj6h09B9TK/RzkrZLA5wx7kO9brERtVAoGAUElcdhKSdQs1r7fVvHeQ\nP+O3i4LEc0Cqs76I+ZEfo43YAdP0UMvKq/k3nUvcnihJePGF6Xh/4TkS2btpUzIs\nZQsMQ0iuL0uyCfYNgxkzl3p3BLV2D5gELOqOuWvPNSi4IZH0LFwjspvMOkPpPpjZ\ng5pceXyF5DVKV5SDajnXoE8=\n-----END PRIVATE KEY-----\n",
        "client_email": "firebase-adminsdk-8r7hq@torre-1df46.iam.gserviceaccount.com",
        "client_id": "114382932175943770838",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-8r7hq%40torre-1df46.iam.gserviceaccount.com"
    }),
    databaseURL: "https://torre-1df46.firebaseio.com"
});

var db = admin.database();
const usersRef = db.ref('/users');



const authenticate = (req, res, next) => { 
    if (!req.headers.btorretoken) {
        res.status(403).send('You shall not pass...');
        return;
    } 
    const idToken = req.headers.btorretoken;
    admin.auth().verifyIdToken(idToken).then(decodedIdToken => { 
        if (decodedIdToken.uid === req.headers.uid) {
            next();
            return; 
        } else {
            res.status(403).send('You shall not pass it.');
        }
       return console.log('UserDecoded: ', decodedIdToken.uid, req.headers.uid);

    }).catch(error => {
        console.log('Err: ', error);
        res.status(403).send('You shall not pass...');
    });
};

app.use( (req, res, next) => {
    // res.setHeader('Access-Control-Allow-Origin', ' https://torre-1df46.web.app');
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
    res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Accept, Content-Type, btorretoken, uid');
    console.log('starting cors config...', res, req);
    next();
});


app.get('/v1/pub/users', (req, res) => {
    usersRef.once('value').then(userSnap => {
        if (userSnap.val()) {
            var users = []
            userSnap.forEach(snap => {
                // console.log(snap);
                users.push(snap);
            })
            res.status(200).send({ status: 200, users: users });
        } else {
            res.status(200).send({ status: 200, msg: "There not users availables" });
        }
        return console.log('This is the notify feature');
    })
        .catch(err => {
            console.log('/v1/pub/users', err);
            res.status(501).send({ status: 501, msg: "The requested operation has not been implemented. A34SW", err: err })
        });

});

app.use(cors);
app.use(authenticate);

// app.post('/v1/priv/user', (req, res) => {

// });

exports.api = functions.https.onRequest(app);